public class AirPlane extends AirVehicle {

    protected int numOfPassengers;

    public AirPlane(String name, int horsePowers, int weight, int numOfWings, int numOfPassengers) {
        super(name, horsePowers, weight, numOfWings);
        this.numOfPassengers = numOfPassengers;
    }
}
