public abstract class AirVehicle extends Vehicle implements LaunchRocketsEnabled{

    protected int numOfWings;

    public AirVehicle(String name, int horsePowers, int weight, int numOfWings) {
        super(name, horsePowers, weight);
        this.numOfWings = numOfWings;
    }

    @Override
    public void launchRockets(AirVehicle airVehicle) {
        System.out.println("ALLAH AKBAR");
        airVehicle.numOfWings--;
        if(weight > 0){
            weight--;
        }
    }
}
