public abstract class Vehicle{
    protected String name;
    protected int horsePowers;
    protected int weight;
    protected int parkingNumber = 0;

    public Vehicle(String name, int horsePowers, int weight) {
        this.name = name;
        this.horsePowers = horsePowers;
        this.weight = weight;
    }


}
