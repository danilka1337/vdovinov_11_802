public abstract class GroundVehicle extends Vehicle implements BeepBeepMotherFather {

    protected int numOfWheels;

    public GroundVehicle(String name, int horsePowers, int weight, int numOfWheels) {
        super(name, horsePowers, weight);
        this.numOfWheels = numOfWheels;
    }
}
