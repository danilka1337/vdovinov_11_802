public interface LaunchRocketsEnabled {
    void launchRockets(AirVehicle vehicle);
}
