abstract public class Parking {
    static private Vehicle[] parking = new Vehicle[20];
    static private int numOfFilled = 0;
    static private int current;

    static public void park(Vehicle vehicle){
        if(numOfFilled < parking.length) {
            for(int i = 0; i < parking.length; i++) {
                if(parking[i] != null){
                    current = i;
                    break;
                }
            }
                parking[current] = vehicle;
                numOfFilled++;
                vehicle.parkingNumber = current + 1;
        }
        else{
            System.out.println("There's no more roome on parking");
        }
    }

    static public void unPark(Vehicle vehicle){
        if(vehicle.parkingNumber != 0){
            parking[vehicle.parkingNumber - 1] = null;
            vehicle.parkingNumber = 0;
        }
        else {
            System.out.println("Your vehicle is not on the parking");
        }
    }

}
