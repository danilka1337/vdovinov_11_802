public interface Driftable {
    void drift();
}
