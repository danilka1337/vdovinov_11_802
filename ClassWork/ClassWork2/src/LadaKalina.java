public class LadaKalina extends GroundVehicle implements Driftable{

    protected int reputation = 0;

    public LadaKalina(String name, int horsePowers, int weight, int numOfWheels) {
        super(name, horsePowers, weight, numOfWheels);
    }


    @Override
    public void drift() {
        System.out.println("Anyway u are looser \n-rep");
        reputation--;
    }

    @Override
    public void beep(){
        System.out.println("...");
    }
}
