public class Helicopter extends AirVehicle {

    protected boolean haveMinigun;

    public Helicopter(String name, int horsePowers, int weight, int numOfWings, boolean haveMinigun) {
        super(name, horsePowers, weight, numOfWings);
        this.haveMinigun = haveMinigun;
    }
}
