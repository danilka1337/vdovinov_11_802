public class Main {

    public static void main(String[] args) {
        SportCar sportCar = new SportCar("2114", 1000,1500,4,10);
        Parking.park(sportCar);
        System.out.println(sportCar.parkingNumber);
        Parking.unPark(sportCar);
    }

}
