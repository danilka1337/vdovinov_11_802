public class Maze {

    public int[][] maze;

    public Maze(int[][] maze){
        this.maze = maze;
    }

    public Maze(int width, int height){
        this.maze = new int[width][height];
        for(int i = 0; i < maze.length; i++){
            for(int j = 0; j < maze[i].length; j++){
                maze[i][j] = -1;
            }
        }
    }

    public void setWall(Cell cell){
        maze[cell.x][cell.y] = -2;
    }

    public void setEmpty(Cell cell){
        maze[cell.x][cell.y] = -1;
    }

    public void show(){
        System.out.print("  ");
        for(int i = 0; i < maze[0].length; i++){
            System.out.print(i + " ");
        }
        System.out.println("y");
        for(int i = 0; i < maze.length;i++){
            System.out.print(i + " ");
            for(int j = 0; j < maze[i].length; j++){
                if(maze[i][j] == -2) {
                    System.out.print("* ");
                }
                else{
                    System.out.print(maze[i][j] + " ");
                }
            }
            System.out.println();
        }
    }



}
