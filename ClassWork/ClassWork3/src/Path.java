import java.util.ArrayList;

public class Path {

    private ArrayList<Cell> path = new ArrayList<>();

    public void run(Maze maze, Cell start, Cell finish){
        boolean isFullParced = false;
        maze.maze[start.x][start.y] = 0;

        while(!isFullParced){
            isFullParced = true;
            for(int x = 0; x < maze.maze.length; x++){
                for(int y = 0; y < maze.maze[x].length; y++){
                    if(maze.maze[x][y] == -1){
                        isFullParced = false;
                    }
                    if(maze.maze[x][y] >= 0){
                        if(maze.maze[x+1][y] != 2 && (maze.maze[x+1][y] == -1 || maze.maze[x+1][y] > maze.maze[x][y] + 1)){
                            maze.maze[x+1][y] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x+1][y+1] != 2 && (maze.maze[x+1][y+1] == -1 || maze.maze[x+1][y+1] > maze.maze[x][y] + 1)){
                            maze.maze[x+1][y+1] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x][y+1] != 2 && (maze.maze[x][y+1] == -1 || maze.maze[x][y+1] > maze.maze[x][y] + 1)){
                            maze.maze[x][y+1] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x-1][y+1] != 2 && (maze.maze[x-1][y+1] == -1 || maze.maze[x-1][y+1] > maze.maze[x][y] + 1)){
                            maze.maze[x-1][y+1] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x-1][y] != 2 && (maze.maze[x-1][y] == -1 || maze.maze[x-1][y] > maze.maze[x][y] + 1)){
                            maze.maze[x-1][y] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x-1][y-1] != 2 && (maze.maze[x-1][y-1] == -1 || maze.maze[x-1][y-1] > maze.maze[x][y] + 1)){
                            maze.maze[x-1][y-1] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x][y-1] != 2 && (maze.maze[x][y-1] == -1 || maze.maze[x][y-1] > maze.maze[x][y] + 1)){
                            maze.maze[x][y-1] = maze.maze[x][y] + 1;
                        }
                        if(maze.maze[x+1][y-1] != 2 && (maze.maze[x+1][y-1] == -1 || maze.maze[x+1][y-1] > maze.maze[x][y] + 1)){
                            maze.maze[x+1][y-1] = maze.maze[x][y] + 1;
                        }
                    }
                }
            }
        }

        Cell current = finish;
        int min=100000;
        int x = 0,y = 0;
        path.add(new Cell(current));
        while(!current.isEquals(start)){
            if(maze.maze[current.x -1][current.y] < min && maze.maze[current.x -1][current.y] != -2){
                min = maze.maze[current.x -1][current.y];
                x=-1;
            }
            if(maze.maze[current.x -1][current.y +1] < min && maze.maze[current.x -1][current.y +1] != -2){
                min = maze.maze[current.x -1][current.y +1];
                x=-1;
                y=1;
            }
            if(maze.maze[current.x][current.y +1] < min && maze.maze[current.x][current.y +1] != -2){
                min = maze.maze[current.x ][current.y +1];
                y=1;
            }
            if(maze.maze[current.x +1][current.y +1] < min && maze.maze[current.x +1][current.y +1] != -2){
                min = maze.maze[current.x +1][current.y +1];
                y=1;
                x=1;
            }
            if(maze.maze[current.x +1][current.y] < min && maze.maze[current.x +1][current.y ] != -2){
                min = maze.maze[current.x +1][current.y];
                x=1;
            }
            if(maze.maze[current.x+1][current.y -1] < min && maze.maze[current.x +1][current.y -1] != -2){
                min = maze.maze[current.x +1][current.y -1];
                y=-1;
                x=1;
            }
            if(maze.maze[current.x][current.y -1] < min && maze.maze[current.x][current.y -1] != -2){
                min = maze.maze[current.x ][current.y -1];
                y=-1;
            }
            if(maze.maze[current.x -1][current.y -1] < min && maze.maze[current.x -1][current.y -1] != -2){
                min = maze.maze[current.x -1][current.y -1];
                y=-1;
                x=-1;
            }

            current.x +=x;
            current.y +=y;
            path.add(new Cell(current));
        }
    }

    public void showPath(){
        for(Cell cell: path){
            System.out.println(cell.x + ";" + cell.y);
        }
    }




}
