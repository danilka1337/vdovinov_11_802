public class Cell {
    public int x;
    public int y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Cell(Cell cell){
        this.x = cell.x;
        this.y = cell.y;
    }

    public boolean isEquals(Cell cell){
        if(this.x == cell.x && this.y == cell.y){
            return true;
        }
        else {
            return false;
        }
    }
}
