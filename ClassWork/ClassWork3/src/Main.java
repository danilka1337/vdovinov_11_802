import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int maze[][] = new int[9][10];

        for(int i = 0; i < maze.length;i++){
            for(int j = 0; j < maze[i].length; j++){
                maze[i][j] = scanner.nextInt();
            }
        }

        Maze maze1 = new Maze(maze);
        Path path = new Path();
        path.run(maze1,new Cell(4,1), new Cell(4,5));
        maze1.show();
        path.showPath();
    }
}
