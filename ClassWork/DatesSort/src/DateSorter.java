import java.io.*;

public class DateSorter {

    public static void sort(String inputFile, String outputFile) throws IOException {
        File file = new File(inputFile);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputFile)));

        IntegerArrayList[] buckets = new IntegerArrayList[61];
        for(int i = 0; i < buckets.length; i++){
            buckets[i] = new IntegerArrayList();
        }

        String line = reader.readLine();
        while (line != null) {
            String[] values = line.split("\\.");
            buckets[Integer.parseInt(values[2]) - 1970].add(Integer.parseInt(values[1])*100 + Integer.parseInt(values[0]));
            line = reader.readLine();
        }

        for(int i = 0; i < buckets.length; i++){
            int array[] = buckets[i].toArray();
            RadixSort.radixsort(array, array.length);
            for(int j = 0; j < array.length; j++){
                int day = array[j] % 100;
                int month = array[j]/100;
                writer.write(day + "." + month + "." + (1970 + i) + "\n");
            }
            writer.flush();
        }

        reader.close();
        writer.close();

    }
}
