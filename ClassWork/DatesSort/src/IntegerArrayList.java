public class IntegerArrayList {
    private int[] mainArray;
    private int size;

    public IntegerArrayList() {
        size = 0;
        mainArray = new int[16];
    }

    public void add(int value){
        if(size == mainArray.length) {
            grow();
        }
        mainArray[size] = value;
        size++;
    }

    private void grow(){
        int[] newArray = new int[size + (size >> 1)];
        System.arraycopy(mainArray, 0,newArray,0,mainArray.length);
        mainArray = newArray;
    }

    public int[] toArray(){
        int[] resultArray = new int[size];
        System.arraycopy(mainArray, 0, resultArray, 0, size);
        return resultArray;
    }

    public int getSize() {
        return size;
    }
}
