import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class DateGenerator {

    public static void generate(int count, String fileName) throws IOException {
        File file = new File(fileName);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        int months[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        Random random = new Random();
        for(int i = 0; i < count; i++){
            int year = 1970 + random.nextInt(61);
            int mouth = 1 + random.nextInt(12);
            int date;
            if(year%4 == 0 && mouth == 2){
                date = 1 + random.nextInt(29);
            } else {
                date = 1 + random.nextInt(months[mouth-1]);
            }
            String s = date + "." + mouth + "." + year + "\n";
            writer.write(s);
            writer.flush();
        }
        writer.close();
    }
}
