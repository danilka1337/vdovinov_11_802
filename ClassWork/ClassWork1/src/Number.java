public class Number {
    protected double a;

    public Number (double a){
        this.a=a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a){
        this.a = a;
    }

    public double getModule(){
        return Math.abs(a);
    }

    public void sout(){
        System.out.println(a);
    }

}
