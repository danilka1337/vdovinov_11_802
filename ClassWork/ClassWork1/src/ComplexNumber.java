public class ComplexNumber extends Number {
    protected double i;

    public ComplexNumber(double a, double i){
        super(a);
        this.i = i;
    }

    public double getI(){
        return i;
    }

    public void setI(double i){
        this.i = i;
    }

    @Override
    public double getModule(){
        return Math.sqrt(a*a + i*i);
    }

    public void sout(){
        if(a != 0){
            System.out.print(a + " ");
        }
        if(i != 0){
            System.out.print(i);
        }
        if(a == 0 && i == 0){
            System.out.println(0);
        }
    }
}
