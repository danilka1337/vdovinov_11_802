public class Quaternion extends ComplexNumber {
    protected double j;
    protected double k;

    public Quaternion(double a, double i, double j, double k){
        super(a,i);
        this.j = j;
        this.k = k;
    }

    public double getJ() {
        return j;
    }

    public void setJ(double j) {
        this.j = j;
    }

    public double getK() {
        return k;
    }

    public void setK(double k) {
        this.k = k;
    }

    @Override
    public double getModule(){
        return Math.sqrt(a*a + i*i + j*j + k*k);
    }

    public void sout(){
        if(a != 0){
            System.out.print(a+" ");
        }
        if(i != 0){
            System.out.print(i+"i ");
        }
        if(j != 0){
            System.out.print(j+"j ");
        }
        if(k != 0){
            System.out.print(k+"k");
        }
        if(a == 0 && i == 0 && j == 0 && k == 0){
            System.out.println(0);
        }
    }
}
