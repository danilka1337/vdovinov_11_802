public class Main {

    public static void reverce(Node head){
        Node var = head.getNext();
        Node last = head;
        Node temp;
        while(var.getNext() != null){
            temp = var.getNext();
            var.setNext(last);
            last = var;
            var = temp;
        }
        head.setNext(null);
        var.setNext(last);
    }

    public static void oddAndEven(Node head){
        Node temp1 = head;
        Node temp2 = head.getNext();
        Node first = head.getNext();
        while(temp1.getNext() != null && temp2.getNext() != null){
            temp1.setNext(temp2.getNext());
            temp1 = temp1.getNext();
            temp2.setNext(temp1.getNext());
            temp2 = temp2.getNext();
        }

        temp1.setNext(first);

    }

    public static void sort(Node head){
        Node temp;
        Node prev = new Node(-1);
        prev.setNext(head);
        Node current = head;
        Node i = head;
            while (current.hasNext()) {
                temp = current.getNext();
                prev.setNext(temp);
                current.setNext(temp.getNext());
                temp.setNext(current);
                prev = prev.getNext();

                current = current.getNext();
            }
    }



    public static void main(String[] args) {

        Node a = new Node(1);
        Node b = new Node(3);
        Node c = new Node(5);
        Node d = new Node(7);
        Node e = new Node(9);
        Node f = new Node(8);
        Node g = new Node(6);
        Node h = new Node(4);
        Node j = new Node(2);

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        e.setNext(f);
        f.setNext(g);
        g.setNext(h);
        h.setNext(j);

        sort(a);

        Node current = a;

        while (current != null) {
            System.out.print(current.getValue() + "->");
            current = current.getNext();
        }
    }
}
