import java.io.*;
import java.util.Scanner;


public class Weather100000 {

    private static String fileName = "/Users/daniilvdovinov/Desktop/main.txt";

    public static void write(String fileName, StringBuilder text){
        File file = new File(fileName);

        try {
            if (!file.exists()) {
                file.createNewFile();
            }


            PrintWriter out = new PrintWriter(file.getAbsoluteFile());

            try {
                out.print(text);
            }
            finally {
                out.close();
            }

        }
        catch(IOException e){
            throw new RuntimeException(e);
        }
    }



    public static void main(String[] args) {

        StringBuilder list = new StringBuilder();
        int lastDay=31;
        int lastMouth=12;
        int lastYear=100;
        int lastTemp=60;
        for(int i=0;i<100000;i++){
            list.append(1+(int)(Math.random()*lastDay)).append(".").append(1+(int)(Math.random()*lastMouth)).append(".").append(1990+(int)(Math.random()*lastYear)).append("\n");
            list.append((int)(Math.random()*lastTemp)-30).append("\n");

        }
        write(fileName,list);

        int arr[] = new int[lastTemp];

        File file = new File(fileName);
        String day="";

        try {
            Scanner in = new Scanner(file.getAbsoluteFile());
            while(in.hasNext()){
                day=in.nextLine();
                arr[30+Integer.parseInt(in.nextLine())]++;
            }
        }
        catch (IOException e){
            throw new RuntimeException(e);
        }

        int daysTemp=0,maxDayTemp=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]>daysTemp) {
                daysTemp = arr[i];
                maxDayTemp = i-30;
            }
        }

        System.out.printf("Максимальное количество дней %d была температура %d",daysTemp,maxDayTemp);

    }
}
