public class Fraction {

    private int numerator;
    private int denominator;

    Fraction(){
        numerator=0;
        denominator=1;
    }

    Fraction(int numerator,int denominator){
        if(numerator==0 || denominator==0) {
            System.out.println("Enter right values \nFraction is 0/1");
            this.numerator = 0;
            this.denominator = 1;
        }
        else{
            this.numerator=numerator;
            this.denominator=denominator;
        }

    }

    Fraction(Fraction obj){
        this.numerator=obj.numerator;
        this.denominator=obj.denominator;
    }

    public int getNumer(){
        return numerator;
    }

    public void setNumer(int a){
        if(a!=0) {
            numerator = a;
        }
        else {
            numerator=0;
            denominator=1;
        }
    }

    public int getDenom(){
        return denominator;
    }

    public void setDenom(int a){
        if(a!=0){
            denominator = a;
        }
        else {
            System.out.println("Enter right denominator");
        }
    }

    public void print(){
        System.out.println(numerator+ "/" + denominator);
    }

    private int gdc(int a, int b){
        while (a!=0 && b!=0){
            if(a > b){
                a = a % b;
            }
            else {
                b = b % a;
            }
        }
        return a+b;
    }

    public void optimize(){
        int del = gdc(numerator,denominator);
        numerator/=del;
        denominator/=del;
    }

    public static Fraction sum(Fraction a,Fraction b){
        int numer = (a.numerator*b.denominator) + (b.numerator*a.denominator);
        int denom = a.denominator*b.denominator;
        Fraction help = new Fraction(numer,denom);
        help.optimize();
        return help;
    }

}
