import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RecursionTest {

    Recursion recursion = new Recursion();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void twoPow() {
        assertEquals(recursion.twoPow(4),"YES");
        assertEquals(recursion.twoPow(16),"YES");
        assertEquals(recursion.twoPow(128),"YES");
        assertEquals(recursion.twoPow(8192),"YES");
        assertEquals(recursion.twoPow(10),"NO");
        assertEquals(recursion.twoPow(63),"NO");
    }

    @Test
    public void isSimple5() {
        assertEquals(recursion.isSimple(5,2),"YES");
    }
    @Test
    public void isSimple7() {
        assertEquals(recursion.isSimple(7,2),"YES");
    }
    @Test
    public void isSimple10() {
        assertEquals(recursion.isSimple(10,2),"NO");
    }
    @Test
    public void isSimple13() {
        assertEquals(recursion.isSimple(13,2),"YES");
    }
    @Test
    public void isSimple991() {
        assertEquals(recursion.isSimple(991,2),"YES");
    }
    @Test
    public void isSimple968207() {
        assertEquals(recursion.isSimple(968207,2),"NO");
    }

    @Test
    public void isPalyndrom1() {
        assertEquals(recursion.isPalyndrom("1"),"YES");
    }
    @Test
    public void isPalyndrom1234321() {
        assertEquals(recursion.isPalyndrom("1234321"),"YES");
    }
    @Test
    public void isPalyndrom123() {
        assertEquals(recursion.isPalyndrom("123"),"NO");
    }
    @Test
    public void isPalyndrom0000() {
        assertEquals(recursion.isPalyndrom("0000"),"YES");
    }
    @Test
    public void isPalyndrom12345432() {
        assertEquals(recursion.isPalyndrom("12345432"),"NO");
    }
}