import java.util.Scanner;

public class Recursion {

    public void from1toN(int n){
        if(n==1){
            System.out.println(n);
        }
        else{
            from1toN(n-1);
            System.out.println(n);
        }
    }

    public void fromAtoB(int a,int b){
        if(a==b){
            System.out.println(a);
        }
        else{
            System.out.println(a);
            if(a<b) {
                fromAtoB(a + 1, b);
            }
            else
            {
                fromAtoB(a - 1, b);
            }
        }
    }

    public int akkerman(int m, int n){
        if(m==0){
            return n+1;
        }
        else if (m > 0 && n==0){
            return akkerman(m-1,1);
        }
        else {
            return akkerman(m-1,akkerman(m,n-1));
        }

    }

    public String twoPow(int a){
        if(a==1){
            return "YES";
        }
        else if(a%2==0){
            return twoPow(a/2);
        }
        else {
            return "NO";
        }
    }

    public int numSum(int a){
        if(a < 10){
            return a;
        }
        else {
            return a % 10 + numSum(a/10);
        }
    }

    public void reverce(int n){
        if(n < 10){
            System.out.print( n + " ");
        }
        else {
            System.out.print(n%10 + " ");
            reverce(n/10);
        }
    }

    public void notReverce(int n){
        if(n < 10){
            System.out.print( n + " ");
        }
        else {
            notReverce(n/10);
            System.out.print(n%10 + " ");

        }
    }

    public String isSimple (int n, int dev){
        if(n % dev==0){
            return "NO";
        }
        else if (dev>Math.sqrt(n)){
            return "YES";
        }
        else return isSimple(n,dev+1);
    }

    public void multipliers(int n, int mul){
        if(mul>Math.sqrt(n)){
            System.out.print(n);
        }
        else if(n % mul==0){
            System.out.println(mul + " ");
            multipliers(n/mul,2);
        }
        else multipliers(n,mul+1);
    }

    public String isPalyndrom (String str){
        if(str.length()<2){
            return "YES";
        }
        else if(str.substring(0,1).equals(str.substring(str.length()-1))){
            return isPalyndrom(str.substring(1,str.length()-1));
        }
        else {
            return "NO";
        }
    }

    public void oddNums(){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return;
        }
        else if (a%2==1){
            System.out.println(a + " ");
            oddNums();
        }
        else {
            oddNums();
        }
    }

    public void oddIndex(){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return;
        }
        System.out.println(a + " ");
        a=scan.nextInt();
        if(a==0){
            return;
        }
        oddIndex();

    }

    public int max(){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return 0;
        }
        else {
            return Math.max(a,max());
        }
    }

    public double average(int sum, int num){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return (double)sum/num;
        }
        else {
            return average(sum+=a,num+=1);
        }
    }

    public int secondMax(int max1,int max2){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return max2;
        }
        else if(a>max1){
            return secondMax(a,max1);
        }
        else if(a>max2){
            return secondMax(max1,a);
        }
        else{
            return secondMax(max1,max2);
        }
    }

    public int countMax(int n, int max){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            return n;
        }
        else if (a > max){
            return countMax(1,a);
        }
        else if (a == max){
            return countMax(n+1,max);
        }
        else {
            return countMax(n,max);
        }
    }

    public int ones (int num){
        Scanner scan = new Scanner(System.in);
        int a=scan.nextInt();
        if(a==0){
            a=scan.nextInt();
            if(a==0){
                return num;
            }
            else if(a==1) {
                return ones(++num);
            }
            else {
                return ones(num);
            }
        }
        else if(a==1){
            return ones(++num);
        }
        else {
            return ones(num);
        }
    }

    public int reverceInt(int n, int i){
        if(n==0){
            return i;
        }
        else {
            return reverceInt(n/10,i*10+n%10);
        }
    }
}
