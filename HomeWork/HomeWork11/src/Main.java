import java.util.Scanner;

public class Main {

    public static int parse(char[] arr){
        int res=0, pow=0,temp;
        for(int i=arr.length-1;i>=0;i--){
            temp=arr[i]-48;
            res+=temp*Math.pow(10,pow);
            pow++;
        }
        return res;
    }

    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);
	    char[] arr={'1','3','5','5','1'};
        System.out.println(parse(arr));
    }
}
