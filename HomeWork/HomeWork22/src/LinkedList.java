public class LinkedList<T> {

    private Node head = null;
    private Node last = null;
    private int count = 0;

    public void add(T value){
        if(head == null){
            head = new Node(value);
            last = head;
        }
        else{
            Node current = head;
            while(current.hasNext()){
                current = current.getNext();
            }
            Node node = new Node(value);
            last = node;
            current.setNext(last);
        }
        count++;
    }


    public void reverce(){
        Node var = head.getNext();
        Node last = head;
        Node temp;
        while(var.getNext() != null){
            temp = var.getNext();
            var.setNext(last);
            last = var;
            var = temp;
        }
        head.setNext(null);
        head = var;
        var.setNext(last);
    }

    public void show(){
        Node current = head;

        while (current.getNext() != null) {
            System.out.print(current.getValue() + "->");
            current = current.getNext();
        }
        System.out.println(current.getValue());
    }

    public T atIndex(int index){
        Node current = head;
        for(int i = 0; i < index; i++){
            if(current == null){
                return null;
            }
            current = current.getNext();
        }
        return current.getValue();
    }

    public int getCount(){
        return count;
    }

    private class Node{
        private T value;
        private Node next = null;
        private Node previous = null;

        public Node(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrevious() {
            return previous;
        }

        public void setPrevious(Node previous) {
            this.previous = previous;
        }

        public boolean hasNext(){
            if(next != null){
                return true;
            }
            else {
                return false;
            }
        }
    }

}
