import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyThread extends Thread {

    private int start;
    private int finish;
    private int[] array;
    private static Lock lock;

    public MyThread(int start, int finish, int[] array){
        this.array = array;
        this.finish = finish;
        this.start = start;
        lock = new ReentrantLock();
    }

    @Override
    public void run(){
        for(int i = start; i <= finish; i++){
            lock.lock();
            Main.expectedSum += array[i];
            lock.unlock();
        }
    }

}

