public class Main {

    public static long sum = 0;
    public static long expectedSum = 0;
    public static int[] array;

    public static void main(String[] args) throws Exception{
        array = new int[1000000];
        int temp;
        for(int i = 0; i < array.length; i++){
            temp = (int)(Math.random()*1000);
            array[i] = temp;
            sum += temp;
        }

        Thread th1 = new MyThread(0, 499999, array);
        Thread th2 = new MyThread(500000, 999999, array);

        th1.start();
        th2.start();

        th1.join();
        th2.join();

        System.out.println(sum);
        System.out.println(expectedSum);


    }
}
