import java.util.Arrays;

class IntegerArrayList {
    private int count;

    private boolean isSorted;

    private int[] mainArray;
    private int[] helpArray;

    IntegerArrayList(int[] mainArray){
        this.mainArray=mainArray;
        count=mainArray.length;
        isSorted=false;
    }

    private void extend(){
        helpArray=mainArray;
        mainArray = new int[
                count < 2 ? count+=1 : count + count/2 ];
        System.arraycopy(helpArray,0,mainArray,0,count);
    }

    public void add(int a){
        if(mainArray.length==count){
            extend();
        }
        if(a < mainArray[count-1]){
            isSorted=false;
        }
        mainArray[count]=a;
        count++;
    }

    public int getCount(){
        return count;
    }

    public void deleteAt(int a){
        if (a >= 0 && a < count){
            for(int i = a;i<count-1;i++){
                mainArray[i]=mainArray[i+1];
            }
            count--;
        }
        else {
            System.out.println("Error. Enter right index");
        }
    }

    public void reverce(){
        int c;
        for(int i = 0; i < count*0.5; i++){
            c=mainArray[i];
            mainArray[i]=mainArray[count-1-i];
            mainArray[count-1-i]=c;
        }
        isSorted=false;
    }

    public int search(int num){
        if(isSorted==false){
            this.sort();
        }
        return bin(0,count-1,num);

    }

    private int bin(int i,int j,int num ) {
        if (num == mainArray[(i + j) / 2]) {
            return (i + j) / 2;
        } else if (j - i <= 1) {
            return -1;
        } else if (num > mainArray[(i + j) / 2]) {
            return bin((i + j) / 2, j, num);
        } else {
            return bin(i, (i + j) / 2, num);
        }
    }


    public void sort(){
        Arrays.sort(mainArray,0,count);
        isSorted=true;
    }

    public void addIntArrayList (IntegerArrayList list, int posIndex){
        if(posIndex >= 0 && posIndex<=mainArray.length) {
            helpArray = mainArray;
            mainArray = new int[helpArray.length + list.mainArray.length];
            for (int i = 0; i < posIndex; i++) {
                mainArray[i] = helpArray[i];
            }
            for(int i = posIndex;i<posIndex+list.mainArray.length;i++){
                mainArray[i]=list.mainArray[i-posIndex];
            }
            for(int i = posIndex+list.mainArray.length; i<mainArray.length;i++){
                mainArray[i]=helpArray[i-list.mainArray.length];
            }
            count+=list.mainArray.length;
            isSorted=false;
        }
        else {
            System.out.println("Enter right index");
        }


    }

    public void sout(){
        for(int i=0;i<count;i++){
            System.out.print(mainArray[i] + " ");
        }
        System.out.println();
    }


}


public class Main {

    public static void main(String[] args) {
        int[] arr = {1,8,6,4,3};
        IntegerArrayList test = new IntegerArrayList(arr);
        test.sort();
        IntegerArrayList help = new IntegerArrayList(arr);
        test.add(1);
        test.addIntArrayList(help,6);
        test.sout();
    }

}
