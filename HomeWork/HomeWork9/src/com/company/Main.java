package com.company;

import java.util.Scanner;

public class Main {

    public static int oddSum(int[] arr){
        int sum=0;
        for(int i = 0; i < arr.length;i++){
            if(i%2==0){
                sum+=arr[i];
            }
        }
        return sum;

    }

    public static void reverse(int[] arr){
        int c;
        for(int i = 0; i < (arr.length)*0.5;i++){
            c=arr[i];
            arr[i]=arr[arr.length-1-i];
            arr[arr.length-1-i]=c;
        }
        for(int i=0;i< arr.length;i++){
            System.out.println(arr[i]);
        }
    }

    public static int localMax(int[] arr){
        int res=0;
        for(int i = 1;i < (arr.length-2);i++){
            if(arr[i]>arr[i-1] & arr[i]>arr[i+1]){
                res++;
            }
        }
        return res;
    }

    public static int func(int a){
        int res=0;
        do{
            res++;
            a/=10;
        }
        while(a!=0);
        return res;
    }

    public static int arrToInt(int[] arr){
        int res=0, pow=0;
        for(int i=arr.length-1;i>=0;i--){
            res+=arr[i]*Math.pow(10,pow);
            pow+=func(arr[i]);
        }
        return res;
    }


    public static void bin(int[] arr, int i, int j, int number){
        if(number==arr[(i+j)/2]){
            System.out.println((i+j)/2);
        }
        else if(j-i<=1){
            System.out.println("error");
        }
        else if(number>arr[(i+j)/2]){
            bin(arr,(i+j)/2,j,number);
        }
        else {
            bin(arr,i,(i+j)/2,number);
        }


    }


    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);

        System.out.println("Длинна массива: ");
        int n=scan.nextInt();
        int[] arr=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=scan.nextInt();
        }

        int choose;

	    while (true){
            System.out.println("Выберите действие: ");
            System.out.println("1. Сумма элементов массива на нечетных позициях");
            System.out.println("2. Развернуть массив и вывести его");
            System.out.println("3. Найти количество локальных максимумов");
            System.out.println("4. Из массива получить int");
            System.out.println("5. Бинарный поиск по массиву");
            System.out.println("0. Выход");

            choose = scan.nextInt();

            switch(choose){
                case 1:
                    {
                        System.out.println(oddSum(arr));
                        break;
                    }
                case 2:
                {
                   reverse(arr);
                   break;
                }
                case 3:
                {
                    System.out.println(localMax(arr));
                    break;
                }
                case 4:
                {
                    System.out.println(arrToInt(arr));
                    break;
                }
                case 5:
                {
                    int c;
                    for(int j = arr.length-1; j>0;j--){
                        for(int i =0;i<j;i++){
                            if(arr[i]>arr[i+1]){
                                c=arr[i];
                                arr[i]=arr[i+1];
                                arr[i+1]=c;
                            }
                        }
                    }
                    int a;
                    System.out.println("Введите элемент, который надо найти: ");
                    a=scan.nextInt();
                    bin(arr,0,arr.length-1,a);
                    break;
                }
                case 0:{
                    System.exit(0);
                }
                default:{
                    System.out.println("Неверное значение");
                }
            }
        }


    }
}
