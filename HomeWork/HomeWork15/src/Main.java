import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void snakeFilled (int[][] Arr){
        Scanner scan = new Scanner(System.in);
        int i=0,j;
        int n=Arr.length;
        int m=Arr[0].length;
        int k=0;

        while (i < n*m)
        {
            k++;
            for (j=k-1;j<m-k+1;j++)
            {
                Arr[k-1][j]=scan.nextInt();
                i++;
            }

            for (j=k;j<n-k+1;j++)
            {
                Arr[j][m-k]=scan.nextInt();
                i++;
            }

            for (j=m-k-1;j>=k-1;j--)
            {
                Arr[n-k][j]=scan.nextInt();
                i++;
            }

            for (j=n-k-1;j>=k;j--)
            {
                Arr[j][k-1]=scan.nextInt();
                i++;
            }
        }
    }

    public static void sortLines (int[][] arr){
        for(int i=0;i<arr.length;i++){
            Arrays.sort(arr[i]);
        }
    }

    public static int[][] sum(int a[][], int b[][]){
        int[][] arr = new int[a.length][a[0].length];
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[0].length;j++){
                arr[i][j]=a[i][j]+b[i][j];
            }
        }
        return arr;
    }

    public static int[][] sub(int a[][], int b[][]){
        int[][] arr = new int[a.length][a[0].length];
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[0].length;j++){
                arr[i][j]=a[i][j]-b[i][j];
            }
        }
        return arr;
    }

    public static int[][] mult(int a[][], int b[][]){
        int[][] arr=new int[a.length][b[0].length];
        int sum=0;
        for(int i=0;i<a.length;i++){
            for(int j=0;j<b[0].length;j++){
                sum=0;
                for(int k=0;k < a[0].length;k++){
                    sum+=a[i][k]*b[k][j];
                }
                arr[i][j]=sum;
            }
        }
        return arr;
    }

    public static void mainDiagonal(int arr[][]){
        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr[i].length;j++){
                arr[i][j]=0;
            }
        }
    }

    public static void otherDiagonal(int arr[][]){
        for(int i=arr.length-1;i>=0;i--){
            for(int j=arr.length-i;j<arr[i].length;j++){
                arr[i][j]=1;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // Просто удалить /*,*/ для запуска нужный части)) Не забыть поставить обратно
        // Заполнение спиралью

        /*int n=scan.nextInt();
        int m=scan.nextInt();
        int[][] arr = new int[n][m];

        snakeFilled(arr);

        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }*/

        // Упорядочение строк
        /*

        int n=scan.nextInt();
        int m=scan.nextInt();
        int[][] arr = new int[n][m];

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                arr[i][j]=scan.nextInt();
            }
        }

        sortLines(arr);

        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }*/

        //Сложение и вычитание матриц. Высота и ширина должна совпадать
        //Смотреть функции ^ ^ ^ там изи

        //Произведение матриц. Высотра первой == ширина второй

        /*int[][] a={{1,2},{0,3},{5,7}};
        int[][] b={{-5,-2,-3},{4,-1,0}};
        int[][] arr=mult(a,b);

        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }*/


        //Главная и побочная диагоняль. Хз нужны ли там только квадратные, работает для всех


        /*int[][] arr={{1,2,3,4,5},{5,7,4,2,7},{8,6,3,1,1},{8,5,2,7,5}};
        //mainDiagonal(arr);
        //otherDiagonal(arr);
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }
        */
    }
}
