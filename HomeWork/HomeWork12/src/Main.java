import java.util.Scanner;

public class Main {

    public static void toLower(char[] arr){
        for(int i = 0;i < arr.length;i++){
            if(arr[i]>=65 && arr[i]<=90){
                arr[i]+=32;
            }
            System.out.println(arr[i]);
        }
    }

    public static void toUpper(char[] arr){
        for(int i = 0;i < arr.length;i++){
            if(arr[i]>=97 && arr[i]<=122){
                arr[i]-=32;
            }
            System.out.println(arr[i]);
        }
    }

    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);
	    char[] arr = {'f','R','g','E','a','Y'};
	    toLower(arr);
	    toUpper(arr);
    }
}
