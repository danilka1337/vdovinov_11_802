import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyScanner {
    private String path;
    private File file;
    private InputStream inputStream;

    public MyScanner(String path) {
        this.path = path;
        try {
            file = new File(path);
            inputStream = new FileInputStream(file);
        }
        catch (IOException e){
            System.err.println("There's no such file");
            System.err.println(e.getMessage());
        }
    }

    public String nextString() throws Exception{
        StringBuffer stringBuffer = new StringBuffer();
        int value = inputStream.read();
        while(value != -1){
            stringBuffer.append((char)value);
            value = inputStream.read();
        }
        return stringBuffer.toString();
    }

    public int nextInt() throws Exception{
        boolean isPositive = true;
        int value = inputStream.read();
        int result = 0;

        if (value == 45) {
            isPositive = false;
            value = inputStream.read();
        } else {
            isPositive = true;
        }

        while(value != -1) {
            result*=10;
            result+=value-48;
            value = inputStream.read();
        }

        return isPositive ? result : -result;
    }

    public double nextDouble() throws Exception {
        boolean isPositive = true;
        boolean isFractionalPart = false;
        int value = inputStream.read();
        int integerResult = 0;
        int fractionalResult = 0;
        double pow = 1;
        double result = 0;

        if (value == 45) {
            isPositive = false;
            value = inputStream.read();
        }

        while (value != -1) {
            if (value == 46) {
                isFractionalPart = true;
                value = inputStream.read();
                continue;
            }
            if (isFractionalPart) {
                fractionalResult *= 10;
                fractionalResult += value - 48;
                pow /= 10;
            }
            else {
                integerResult *= 10;
                integerResult += value - 48;

            }
            value = inputStream.read();
        }

        result = integerResult + fractionalResult*pow;
        return isPositive ? result : result*(-1);

    }
}
