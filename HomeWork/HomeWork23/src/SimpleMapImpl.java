public class SimpleMapImpl<K, V> implements Map<K, V>{

    private static class Node<K, V>{
        private final K key;
        private V value;
        private Node next;

        private Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public boolean hasNext(){
            if(next != null){
                return true;
            }
            else {
                return false;
            }
        }

        private Node getNext(){
            return next;
        }

        private void setNext(Node node){
            next = node;
        }
    }

    private Node nodes[];


    public SimpleMapImpl() {
        nodes = new Node[16];
    }

    private int getIndex(K key){
        return (nodes.length - 1) & key.hashCode();
    }

    @Override
    public V get(K key) {
        int index = getIndex(key);
        if(nodes[index] == null){
            return null;
        }
        else {
            Node current = nodes[index];
            while(current.hasNext()){
                current = current.getNext();
            }
            return (V)current.value;
        }
    }

    @Override
    public void put(K key, V value) {
        Node current = nodes[getIndex(key)];
        if(current == null){
            nodes[getIndex(key)] = new Node<>(key, value);
        }
        else {
            while(current.hasNext()){
                if(current.key.hashCode() == key.hashCode()){
                    current.value = value;
                    return;
                }
                current = current.getNext();
            }
            current.setNext(new Node<>(key, value));
        }
    }
}
