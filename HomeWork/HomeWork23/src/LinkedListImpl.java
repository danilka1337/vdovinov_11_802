public class LinkedListImpl<K, V> implements LinkedList<K, V> {

    private Node head = null;
    private int count = 0;

    @Override
    public void add(K key, V value) {
        if(head == null){
            head = new Node(key, value);
        }
        else{
            Node current = head;
            while(current.hasNext()){
                current.getNext();
            }
            current.setNext(new Node(key, value));
        }
        count++;
    }

    public V find(K key){
        Node current = head;
        while (current != null) {
            if(current.getHash() == key.hashCode()){
                return current.value;
            }
            current = current.getNext();
        }
        return null;

    }

    private class Node {
        private K key;
        private V value;
        private Node next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public boolean hasNext(){
            if(this.next != null){
                return true;
            }
            else{
                return false;
            }
        }

        private Node getNext(){
            return next;
        }

        private int getHash(){
            return this.key.hashCode();
        }

        private void setNext(Node node){
            this.next = node;
        }
    }
}
