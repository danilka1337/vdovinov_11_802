public interface LinkedList<K, V> {
    void add(K key, V value);
    V find(K key);
}
