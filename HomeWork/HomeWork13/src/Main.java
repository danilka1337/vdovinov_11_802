import java.util.Scanner;

public class Main {

    public static int fib(int a, int b, int n){
        if(n==0){
            return a;
        }
        else {
            return fib(b,a+b,n-1);
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println(fib(0,1,scan.nextInt()));
    }
}
