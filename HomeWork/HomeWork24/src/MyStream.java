import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyStream<S> {
    MyLinkedList<S> list;

    public MyStream(MyLinkedList<S> list) {
        this.list = list;
    }

    public MyStream<S> filter(Predicate<S> p){
        MyLinkedList<S> tempList = new MyLinkedList<>();
        for(S value : list){
            if(p.test(value)){
                list.add(value);
            }
        }
        return new MyStream<>(tempList);
    }

    public <R> MyStream<R> map(Function<S, R> f){
        MyLinkedList<R> tempList = new MyLinkedList<>();
        for(S value : list){
            tempList.add(f.apply(value));
        }
        return new MyStream<>(tempList);
    }

    public void forEach(Consumer<S> c){
        for(S value : list){
            c.accept(value);
        }
    }

}
