import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        MyLinkedList<String> list = new MyLinkedList<>();
        list.add("Damir");
        list.add("Bulat");
        list.add("Bomj");
        list.add("Java");
        list.add("Sharp");
        list.stream().map(String::length).forEach(System.out::println);
    }
}
