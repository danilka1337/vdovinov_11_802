import java.util.Iterator;

public class MyLinkedList<L> implements Iterable<L>{

    private Node<L> head;
    private Node<L> last;
    private int size = 0;

    private class Node<L> {

        private L value;
        private Node<L> next;

        Node(L value) {
            this.value = value;
        }

    }

    private class LinkedListIterator implements Iterator<L> {

        private Node<L> current = head;
        Node<L> temp;
        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public L next() {
            temp = current;
            current = current.next;
            index++;
            return temp.value;
        }
    }

    @Override
    public Iterator<L> iterator() {
        return  new LinkedListIterator();
    }

    public void add(L value){
        if(size == 0){
            head = new Node<>(value);
            last = head;
        }else {
            last.next = new Node<>(value);
            last = last.next;
        }
        size++;
    }

    public MyStream<L> stream(){
        return new MyStream<>(this);
    }

}
