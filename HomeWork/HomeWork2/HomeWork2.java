import java.util.Scanner;

public class HomeWork2{

    public static int sum(int a){
        int sum=0;
        while(a!=0){
            sum+=a%10;
            a/=10;
        }
        return sum;
    }

    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int res=1;
        int in;
        do{
            in=scanner.nextInt();
            if(sum(in)%2==1){
                res*=in;
            }
        }
        while(in!=-1);
        System.out.println(res);
    }
}