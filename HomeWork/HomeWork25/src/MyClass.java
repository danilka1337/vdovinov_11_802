public class MyClass {
    private Integer num;
    private String name;

    public MyClass(int num, String name) {
        this.num = num;
        this.name = name;
    }

    public void printInfo(){
        System.out.println(name + " " + num);
    }
}
