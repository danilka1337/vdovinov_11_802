import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Framework {

    public static <T> List<T> getObjects(int count, Class<T> aClass, Object... args){
        Class[] params = new Class[args.length];
        for(int i = 0; i < args.length; i++){
            if(args[i] instanceof Integer){
                params[i] = int.class;
                continue;
            }
            if(args[i] instanceof Double){
                params[i] = double.class;
                continue;
            }
            if (args[i] instanceof Boolean){
                params[i] = boolean.class;
                continue;
            }
            if(args[i] instanceof Long){
                params[i] = long.class;
                continue;
            }
            if(args[i] instanceof Character){
                params[i] = char.class;
                continue;
            }
            if(args[i] instanceof Float){
                params[i] = float.class;
                continue;
            }
            if(args[i] instanceof Short){
                params[i] = short.class;
                continue;
            }
            if(args[i] instanceof Byte){
                params[i] = byte.class;
                continue;
            }
            params[i] = args[i].getClass();

        }
        List<T> list = new ArrayList<>(count);
        try {
            T obj = aClass.getConstructor(params).newInstance(args);
            for(int i = 0; i < count; i++){
                list.add(obj);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return list;

    }
}
