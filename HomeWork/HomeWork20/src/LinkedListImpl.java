public class LinkedListImpl {
    private Node head;
    private Node tail;
    private int count = 1;

    public LinkedListImpl(Node head) {
        this.head = head;
        Node current = head;
        while(current.hasNext()){
            count++;
            current = current.getNext();
        }
        tail = current;
    }

    public LinkedListImpl(){
        head = null;
        tail = null;
        count = 0;
    }

    public void add(Node node){
        if(head == null){
            head = node;
            tail = node;
        }else {
            node.setPrevious(tail);
            tail.setNext(node);
            tail = node;
        }
        count++;
    }

    public int getCount() {
        return count;
    }

    public void reverce(){
        Node var = head.getNext();
        Node last = head;
        Node temp;
        while(var.getNext() != null){
            temp = var.getNext();
            var.setNext(last);
            last = var;
            var = temp;
        }
        head.setNext(null);
        var.setNext(last);
    }

    public void remove(int index){
       if(index < 0 || index > count - 1){
           System.out.println("Куда пошёл ????");
       }
       else if(index == 0) {
           head = head.getNext();
           count--;
       }
       else{
           Node prev = head;
           for(int i = 0; i < index - 1; i++){
               prev = prev.getNext();
           }
           prev.setNext(prev.getNext().getNext());
           count--;
       }
    }

    public void clear(){
        head = null;
        tail = null;
        count = 0;
    }

    public void sort(){
        boolean swapped = false;
        while(!swapped) {
            Node current = head;
            swapped = true;
            while (current.getNext() != null) {
                if (current == head && current.getValue() > current.getNext().getValue()) {
                    current.getNext().getNext().setPrevious(current);
                    current.setPrevious(current.getNext());
                    current.setNext(current.getNext().getNext());
                    current.getPrevious().setNext(current);
                    current.getPrevious().setPrevious(null);
                    head = current.getPrevious();
                    swapped = false;

                } else if (current.getNext().getNext() == null && current.getValue() > current.getNext().getValue()) {
                    current.getPrevious().setNext(current.getNext());
                    current.getNext().setPrevious(current.getPrevious());
                    current.getNext().setNext(current);
                    current.setPrevious(current.getNext());
                    current.setNext(null);
                    tail = current;
                    swapped = false;

                } else if (current.getValue() > current.getNext().getValue()) {
                    current.getPrevious().setNext(current.getNext());
                    current.getNext().getNext().setPrevious(current);
                    current.getNext().setPrevious(current.getPrevious());
                    current.setPrevious(current.getNext());
                    current.setNext(current.getNext().getNext());
                    current.getPrevious().setNext(current);
                    swapped = false;

                } else {
                    current = current.getNext();
                }
            }
        }

    }

    public void show(){
        Node current = head;

        while (current.getNext() != null) {
            System.out.print(current.getValue() + "->");
            current = current.getNext();
        }
        System.out.println(current.getValue());
    }

    private class LinkedListIterator implements Iterator {

        private int currentIndex;
        private Node current;

        public LinkedListIterator() {
            this.currentIndex = 0;
            this.current = head;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }

        @Override
        public Object next() {
            Object result = current;
            current = current.getNext();
            currentIndex++;
            return result;
        }
    }

    public Iterator getIterator(){
        return new LinkedListIterator();
    }

}
