public class Node {
    private int value;
    private Node next = null;
    private Node previous = null;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public boolean hasNext(){
        if(next != null){
            return true;
        }
        else{
            return false;
        }
    }
}
