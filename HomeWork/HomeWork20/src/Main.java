public class Main {

    public static void main(String[] args) {
        LinkedListImpl list = new LinkedListImpl();
        list.add(new Node(1));
        list.add(new Node(2));
        list.add(new Node(3));
        list.add(new Node(-1));
        list.add(new Node(-2));
        list.sort();
        list.show();

    }
}
