package com.company;

import java.util.Scanner;

public class Main {

    public static double func (double x){
        return x*x*x+2*x*x+5*x+6 ;
    }

    public static double leftRect(double a, double b, int n){
        double res=0;
        double h=(b-a)/n;
        for(double i=a;i<b;i+=h){
            res+=func(i)*h;
        }
        return res;
    }

    public static double rightRect(double a,double b, int n){
        double res=0;
        double h=(b-a)/n;
        for(double i=a+h;i<=b;i+=h){
            res+=func(i)*h;
        }
        return res;
    }

    public static double midRect(double a,double b, int n){
        double res=0;
        double h=(b-a)/n;
        for (double i=(a+h)*0.5;i<b;i+=h){
            res+=func(i)*h;
        }
        return res;
    }

    public static double trapeze(double a, double b, int n){
        double res=0;
        double h=(b-a)/n;
        for(double i=a;i<b;i+=h){
            res+=((func(i)+func(i+h))*0.5)*h;
        }
        return res;
    }

    public static double simpson(double a, double b){
        return ((b-a)/6)*(func(a)+func(b)+4*func((a+b)*0.5));
    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double a = scan.nextDouble();
        double b = scan.nextDouble();
        int n = scan.nextInt();
        System.out.print("Метод левых прямоугольников: ");
        System.out.println(leftRect(a,b,n));
        System.out.print("Метод правых прямоугольников: ");
        System.out.println(rightRect(a,b,n));
        System.out.print("Метод средних прямоугольников: ");
        System.out.println(midRect(a,b,n));
        System.out.print("Метод трапеции: ");
        System.out.println(trapeze(a,b,n));
        System.out.print("По формуле Симпсона: ");
        System.out.println(simpson(a,b));

    }
}
