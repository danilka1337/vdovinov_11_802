import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static String[] sliceWords(String string){
        string = string.replace(".","");
        string = string.replace(",","");
        string = string.replace(":","");
        string = string.replace("-","");
        string = string.replace("?","");
        string = string.replace("!","");
        string = string.replace("  "," ");

        string = string.toLowerCase();

        return string.split(" ");
    }

    public static String[] deleteCopies(String[] array){
        String[] helpArray = new String[array.length];
        int currentIndex=1;
        helpArray[0] = array[0];
        for(int i = 1; i < array.length; i++){
            if(!(array[i].equals(helpArray[currentIndex-1]))){
                helpArray[currentIndex++]=array[i];
            }
        }
        String[] resultArray = new String[currentIndex];
        for(int i = 0; i < currentIndex; i++){
            resultArray[i]=helpArray[i];
        }
        return resultArray;

    }

    public static int matchesInStringArray(String str, String[] array){
        int res = 0;
        for(int i = 0; i < array.length;i++){
            if(array[i].equals(str)){
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String stringA = scanner.nextLine();
        String stringB = scanner.nextLine();

        String[] stringArrayA = sliceWords(stringA);
        String[] stringArrayB = sliceWords(stringB);

        Arrays.sort(stringArrayA);
        Arrays.sort(stringArrayB);

        String[] stringArrayC = new String[ stringArrayA.length + stringArrayB.length];

        for(int i = 0; i < (stringArrayA.length + stringArrayB.length);i++){
            if(i<stringArrayA.length){
                stringArrayC[i]=stringArrayA[i];
            }
            else {
                stringArrayC[i]=stringArrayB[i-stringArrayA.length];
            }
        }

        Arrays.sort(stringArrayC);

        System.out.println(Arrays.toString(stringArrayC));

        stringArrayC = deleteCopies(stringArrayC);

        System.out.println(Arrays.toString(stringArrayC));

        int[] vectorA = new int[stringArrayC.length];
        int[] vectorB = new int[stringArrayC.length];

        for(int i = 0; i<stringArrayC.length; i++){
            vectorA[i]=matchesInStringArray(stringArrayC[i],stringArrayA);
        }
        for(int i = 0; i<stringArrayC.length; i++){
            vectorB[i]=matchesInStringArray(stringArrayC[i],stringArrayB);
        }

        System.out.println(Arrays.toString(vectorA));
        System.out.println(Arrays.toString(vectorB));

        double multSum = 0;
        double a2Sum = 0;
        double b2Sum = 0;

        for(int i = 0; i < stringArrayC.length; i++){
            multSum+=vectorA[i]*vectorB[i];
            a2Sum+=vectorA[i]*vectorA[i];
            b2Sum+=vectorB[i]*vectorB[i];
        }

        double res = (multSum)/(Math.sqrt(a2Sum*b2Sum));
        System.out.println("Совпадение: " + res*100);
    }
}
