public class Ellipse extends Figure implements Perimeter, Squarable{

    protected static final double pi = 3.1415926535897;

    protected double a;
    protected double b;

    public Ellipse(double x, double y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return 2*pi*Math.sqrt((a*a + b*b)*0.5);
    }

    @Override
    public double getSquare() {
        return pi*a*b;
    }
}
