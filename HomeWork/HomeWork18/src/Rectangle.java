public class Rectangle extends Parallelogram {

    public Rectangle(double x, double y, double side1, double side2) {
        super(x, y, side1, side2);
    }

    @Override
    public double getSquare() {
        return side1*side2;
    }
}
