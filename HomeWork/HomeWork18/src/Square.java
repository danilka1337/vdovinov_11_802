public class Square extends Rectangle {

    public Square (double x, double y, double side1){
        super(x, y, side1, side1);
    }

    @Override
    public double getSquare() {
        return side1*side1;
    }
}
