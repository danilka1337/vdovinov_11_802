public class Rhombus extends Parallelogram {

    public Rhombus(double x, double y, double side1) {
        super(x, y, side1, side1);
    }

    @Override
    public double getPerimeter() {
        return side1*4;
    }
}
