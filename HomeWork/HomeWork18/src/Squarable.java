public interface Squarable {
    double getSquare();
}
