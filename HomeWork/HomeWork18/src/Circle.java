public class Circle extends Ellipse {


    public Circle(double x, double y, double r) {
        super(x, y, r, r);
    }

    @Override
    public double getPerimeter() {
        return 2*pi*a;
    }

    @Override
    public double getSquare() {
        return pi*a*a;
    }
}
