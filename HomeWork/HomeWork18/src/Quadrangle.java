public class Quadrangle extends Polygon implements Perimeter, Squarable{

    protected double side1;
    protected double side2;
    protected double side3;
    protected double side4;

    public Quadrangle(double x, double y, double side1, double side2, double side3, double side4) {
        super(x, y);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.side4 = side4;
    }

    public double getSide1() {
        return side1;
    }

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public double getSide2() {
        return side2;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    public double getSide3() {
        return side3;
    }

    public void setSide3(double side3) {
        this.side3 = side3;
    }

    public double getSide4() {
        return side4;
    }

    public void setSide4(double side4) {
        this.side4 = side4;
    }

    @Override
    public double getPerimeter() {
        return side1+side2+side3+side4;
    }


    @Override
    public double getSquare() {
        double halfPeriemeter = getPerimeter()*0.5;
        return Math.sqrt((halfPeriemeter - side1)*(halfPeriemeter - side2)*(halfPeriemeter - side3)*(halfPeriemeter - side4));
    }
}
