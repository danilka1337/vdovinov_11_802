public class HomeWork7{

	static int func(int a){
		int res=0;
		while(a!=0){
			res++;
			a/=10;
		}
		return res;
	}

	public static void main(String args[]){
		int arr[]={1,6,45,2,8,2,6};
		int res=0, pow=0;
		for(int i=arr.length-1;i>=0;i--){
			res+=arr[i]*Math.pow(10,pow);
			pow+=func(arr[i]);
		}
		System.out.println(res);
	}

}