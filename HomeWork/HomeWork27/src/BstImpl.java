public class BstImpl<T extends Comparable<T>> implements Tree<T>{

    class Node{
        private T value;
        private Node left;
        private Node right;

         public Node(T value) {
             this.value = value;
         }
    }

    private Node root;

    public BstImpl() {
        this.root = null;
    }

    @Override
    public void insert(T value) {
        this.root = insert(root,value);
    }

    private Node insert(Node root, T value){
        if(root == null){
            root = new Node(value);
        }
        else if (value.compareTo(root.value) <= 0 ){
            root.left = insert(root.left,value);
        }
        else {
            root.right = insert(root.right, value);
        }
        return root;
    }

    @Override
    public boolean remove(T value) {
        Node temp = find(this.root, value);
        if(temp == null){
            return false;
        }
        else {
            Node left = temp.left;
            Node right = temp.right;
            temp = null;
            insert(left);
            insert(right);
            return true;
        }
    }

    private void insert(Node root){
        if(root != null) {
            this.insert(root.value);
            insert(root.left);
            insert(root.right);
        }
    }

    private Node find(Node root,T value){
        if(root == null){
            return null;
        }
        else if(root.value == value){
            return root;
        }
        else {
            if(value.compareTo(root.value) > 0){
                return find(root.right, value);
            }
            else {
                return find(root.left, value);
            }
        }
    }

    @Override
    public void print() {
        print(this.root);
    }

    private void print(Node root){
        if(root != null){
            print(root.left);
            System.out.print(root.value + " ");
            print(root.right);
        }
    }

    @Override
    public boolean contains(T value) {
        return contains(this.root, value);
    }

    private boolean contains(Node root, T value){
        if(root == null){
            return false;
        }
        else if(root.value == value){
            return true;
        }
        else if(value.compareTo(root.value) <= 0){
            return contains(root.left, value);
        }
        else {
            return contains(root.right, value);
        }
    }

    @Override
    public void printByLevels() {

    }

    @Override
    public boolean isBst() {
        return isBst(this.root);
    }

    private boolean isBst(Node root){
        if(root.left != null && root.right != null){
            return isBst(root.left) && isBst(root.right);
        }
        else if(!(root.left != null || root.right != null)){
            return true;
        }
        else if(root.left == null){
            return isBst(root.right);
        }
        else {
            return isBst(root.left);
        }

    }
}

